<?php
	date_default_timezone_set('America/New_York');
	
	$dbserver = @$_ENV{'DATABASE_SERVER'} ? $_ENV{'DATABASE_SERVER'} : 'localhost';
	$config_file = file_get_contents('wp-config.php'); 
	$config_file = preg_replace('/\$_ENV{DATABASE_SERVER}/sx',"'localhost'",$config_file);
	
	preg_match_all("/.*define\('([^']+)', '([^']+).*/", $config_file, $cfg);
	
	foreach($cfg[1] as $k=>$v){
		$config[$cfg[1][$k]] = $cfg[2][$k];
		if($cfg[1][$k] == 'DB_HOST'){break;}
	}
	
	if( empty($config['DB_USER']) || empty($config['DB_PASSWORD']) || empty($config['DB_NAME']) ) {
		echo "Database :: Credentials ERROR\n"; break;
	} else {
		echo "Database :: executing ''mysqldump -u $config[DB_USER] -p$config[DB_PASSWORD] -h $dbserver $config[DB_NAME] > database.bak.sql'\n";
		$ans = system("mysqldump -u $config[DB_USER] -p$config[DB_PASSWORD] -h $dbserver $config[DB_NAME] > database.bak.sql",$r);
		echo "Database :: Saved as database.bak.sql\n";
		
		$date = date('Ymd.h.i.s');
		$file = 'backup.' . $date . '.tgz';		
		$ans = system('tar -czpf ' . $file . ' --exclude=\'*.tgz\' --exclude=\'*pack*.php\'  *',$r);
		echo "File Sys :: Packed as $file\n";		
	}
?>