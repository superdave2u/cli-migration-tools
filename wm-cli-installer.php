<?php

	if (!file_exists('wp-config.php') || !file_exists('wp-config.php') ) {
		echo "Missing required files (config or db)..\n";
		die;
	}

	date_default_timezone_set('America/New_York');
	
	$dbserver = @$_ENV{'DATABASE_SERVER'} ? $_ENV{'DATABASE_SERVER'} : 'localhost';
	
	$config_file = file_get_contents('wp-config.php'); 
	
	preg_match_all("/.*define\('([^']+)', '([^']+).*/", $config_file, $cfg);
	
	foreach($cfg[1] as $k=>$v){
		$config[$cfg[1][$k]] = $cfg[2][$k];
		if($cfg[1][$k] == 'DB_HOST'){break;}
	}
	
	if( empty($config['DB_USER']) || empty($config['DB_PASSWORD']) || empty($config['DB_NAME']) ) {
		echo "Database :: Credentials ERROR\n"; break;
	} else {
		echo "Database :: Ecexuting 'mysql -u $config[DB_USER] -p$config[DB_PASSWORD] -h $dbserver $config[DB_NAME] < database.bak.sql'\n";
		$ans = system("mysql -u $config[DB_USER] -p$config[DB_PASSWORD] -h $dbserver $config[DB_NAME] < database.bak.sql",$r);
		echo "Database :: Command Executed:\n\t$ans\n";
	}
?>