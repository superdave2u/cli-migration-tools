<?php
	date_default_timezone_set('America/New_York');
	
	$action = (@empty($argv[1])) ? NULL : $argv[1];

	// config action
	$action = (!empty($_GET['a']) && empty($action)) ? $_GET['a'] : '';
	
	// check if there's a backup in current dir	
	$backupfiles = $sqlfiles = array();
	
	if($action == '') {
		$dir    = './';
		$filesArr = scandir($dir);
		foreach($filesArr as $file) {
			if(!is_dir($file)) {
				if(findexts($file) == 'tgz') {
					$backupfiles[] = $file;
				}
				if(findexts($file) == 'sql') {
					$sqlfiles[] = $file;
				}
			}
		}
	}
	
	if (count($backupfiles) || count($sqlfiles)) {
		echo "<h1>Backup files found</h1><i>Please choose an option...</i><BR><BR>";
		foreach ($backupfiles as $file) {
			echo "Restore <a href='". $_SERVER['PHP_SELF'] . "?a=restore&file=" . urlencode(str_replace('.tgz','',$file)) . "' target='_BLANK'>$file</a><BR>";
		}
		foreach ($sqlfiles as $file) {
			echo "Restore <a href='". $_SERVER['PHP_SELF'] . "?a=restoresql&file=" . urlencode(str_replace('.sql','',$file)) . "' target='_BLANK'>$file</a><BR>";
		}
		
		echo "<BR><BR>or <a href='". $_SERVER['PHP_SELF'] . "?a=skipRestore'>Skip Restore</a>";
	} else {
	
		// set up database variables
		
		$dbserver = $_ENV{'DATABASE_SERVER'}?$_ENV{'DATABASE_SERVER'}:'localhost';
		$config_file = file_get_contents('wp-config.php'); 
		$config_file = preg_replace('/\$_ENV{DATABASE_SERVER}/sx',"'localhost'",$config_file);
		
		// preg_match_all("/.*define\('([^']+)'[^']+'([^']+).*/"     ,$config_file,$cfg); //updated this to fix the wp_cache issue
		preg_match_all("/.*define\('([^']+)', '([^']+).*/", $config_file, $cfg);
		
		foreach($cfg[1] as $k=>$v){
			$config[$cfg[1][$k]] = $cfg[2][$k];
			if($cfg[1][$k] == 'DB_HOST'){break;} // last one we'll need to define
		}
		
		
		if ($action == 'pack') {
			$name = (!empty($_POST["name"])) ? $_POST["name"] : '';

			$ans = system("mysqldump -u $config[DB_USER] -p$config[DB_PASSWORD] -h $dbserver $config[DB_NAME] > database.bak.sql",$r);
			print "Database :: Saved as database.bak.sql<br>";
			
			$date = date('Ymd.h.i.s');
			$file = $name . '.bak.' . $date . '.tgz';		
			$ans = system('tar -czpf ' . $file . ' --exclude=\'*.tgz\' --exclude=\'wp-pack.php\'  *',$r);
			print "File Sys :: Packed as $file<br>";
		
			echo "<a href='". $_SERVER['PHP_SELF'] . "?a=dl&file=" . urlencode(str_replace('.tgz','',$file)) . "' target='_BLANK'>Download Here</a>";
			
		} else if ($action == 'dl') {
			$file = (!empty($_GET["file"])) ? $_GET["file"] . '.tgz' : NULL;
			if(is_null($file))
				die("ERROR WITH FILE");
			
			_Download($file , $file);
			
		} else if ($action == 'restore') {
			$file = (!empty($_GET["file"])) ? $_GET["file"] . '.tgz' : NULL;
			if(is_null($file) || !is_file($file))
				die("ERROR WITH FILE");
			
			_Restore($file , $file);
			echo "Restore completed - <a href='http://". $_SERVER['HTTP_HOST'] ."'>Click Here</a>";
			
		} else if ($action == 'restoresql') {
			$file = (!empty($_GET["file"])) ? $_GET["file"] . '.sql' : NULL;
			
			if(is_null($file) || !file_exists($file))
				die("ERROR WITH FILE");
			
			_RestoreSQL($file , $file);
			echo "Restore completed - <a href='http://". $_SERVER['HTTP_HOST'] ."'>Click Here</a>";
			
		} else if ($action == 'domains') {
			/*			
			Figure out a way to run these SQL statements after reading the new domain, old domain, and table prefix...
			
			UPDATE wp_posts SET guid = replace(guid, from_domain,to_domain);
			UPDATE wp_posts SET post_content = replace(post_content, from_domain,to_domain);
			UPDATE wp_options SET option_value = REPLACE( option_value, from_domain,  to_domain) ;
			UPDATE wp_postmeta SET meta_value = REPLACE( meta_value, from_domain,  to_domain) ;
			
			*/
		} else {
		
		?>
			Domain:
			<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>?a=pack">
				<input type="text" name="name" value="<?php echo $_SERVER['HTTP_HOST'];?>" size="70">
				<input type="Submit" value="Backup">
			</form>
			<i>
			<?php
			print "You'll need a new wp-config.php <BR>or match these settings:";
			print "<pre>". print_r($config,1).'</pre>';	
			
		}
	}
	
	/*
	
	HELP FUNCTIONS 
	
	*/
	
	// returns file extension
	function findexts ($filename) 	{ 
		$filename = strtolower($filename) ; 
		$exts = split("[/\\.]", $filename) ; 
		$n = count($exts)-1; 
		$exts = $exts[$n]; 
		return $exts; 
	}

	// config download action
	function _Download($f_location, $f_name){
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Length: ' . filesize($f_location));
		header('Content-Disposition: attachment; filename=' . basename($f_name));
		readfile($f_location);
	}
	
	function _Restore($f_location, $f_name){
		if (file_exists($f_location)) {
			
			// backup wp config - we'll need these DB settings 
			if(file_exists('wp-config.php')) 
				system("mv wp-config.php wp-config.bak.php",$r);

			// expand file system
			$ans = system("tar -xzpf " . $f_location,$r);

			// update file permissions
			 $ans = system("chown webmanna.ftpusers * -R",$r);
			 $ans = system("chmod g+w * -R",$r);
		}
	}
	function _RestoreSQL($f_location, $f_name){
		if (file_exists($f_location)) {
			
			$config_file = file_get_contents('wp-config.php'); 
			$config_file = preg_replace('/\$_ENV{DATABASE_SERVER}/sx',"'localhost'",$config_file);
			preg_match_all("/.*define\('([^']+)'[^']+'([^']+).*/"     ,$config_file,$cfg);
			foreach($cfg[1] as $k=>$v){
				$config[$cfg[1][$k]] = $cfg[2][$k];
				if($cfg[1][$k] == 'DB_HOST'){break;}
			}
			
			echo "Config: <br><pre>". print_r($config,1) ."</pre>";
			
			## Connect to a local database server (or die) ## 
			$dbH = @mysql_connect($config[DB_HOST], $config[DB_USER], $config[DB_PASSWORD]) 
				or die('Could not connect to MySQL server.<br><I>' . mysql_error() . '</I>'); 

			## Select the database to insert to ## 
			@mysql_select_db($config[DB_NAME]) 
				or die('Could not select database.<br><I>' . mysql_error() . '</I>'); 

			ProcessSQL('database.bak.sql');
			
			## Close database connection when finished ## 
			@mysql_close($dbH); 

			// import sql dump
			// $ans = system("mysql -u ". $config[DB_USER]." -p".$config[DB_PASSWORD]." -h ".$dbserver." ".$config[DB_NAME]." < database.bak.sql",$r);
			// echo "<pre>".print_r(array($ans,$r),1) . "</pre>";
		}
	}
	function ProcessSQL($file, $delimiter = ';')
	{
		set_time_limit(0);
		$res = array();
		
		if (is_file($file) === true)
		{
			$file = fopen($file, 'r');

			if (is_resource($file) === true)
			{
				$query = array();

				while (feof($file) === false)
				{
					$query[] = fgets($file);
					
					if (preg_match('~' . preg_quote($delimiter, '~') . '\s*$~iS', end($query)) === 1)
					{
						$query = trim(implode('', $query));

						if (mysql_query($query) === false)
						{
							$res['ERROR'] = $query;
						}

						else
						{
							$res['SUCCESS']= $query;
						}

						while (ob_get_level() > 0)
						{
							ob_end_flush();
						}

						flush();
					}

					if (is_string($query) === true)
					{
						$query = array();
					}
				}
				if(count($res)) {
						echo "Confirms(".$res['SUCCESS'].")";
						foreach($res['ERROR'] as $error) {
							echo htmlspecialchars($error) . "<BR>\n";
						}
				}
				return fclose($file);
			}
		}
		return false;
	}	
?>